importScripts('pako.min.js');

addEventListener('message', function(e) {
	var fileObj = e.data.fileObject;

    // unpack zip
    var xmlString = pako.ungzip(fileObj, {to: 'string'});

    // var parser = new DOMParser();
    // var xml = parser.parseFromString( xmlString , "text/xml" );

	// use jQuery to parse xml as DOM
    // var $xml =  $( $.parseXML(xmlString) );

	// get tempo value from xml
    // var tempo = $xml.find('Tempo Manual').attr('Value');

	postMessage({
		'tempo': xmlString
	});
});