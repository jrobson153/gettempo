var getTempo = {
	init: function() {

		getTempo.dropListener();

	},

	dropListener: function(){

		var obj = $(".site-wrapper");

		// listen for file drag
		obj.on('dragenter', function(e){

		    e.stopPropagation();
		    e.preventDefault();

		    $('body').addClass('dragging');

		});

		// listen for dragover
		obj.on('dragover', function(e){

			// kill default behavior
			e.stopPropagation();
			e.preventDefault();

		});

		// listen for file/folder drop
		obj.on('drop', function(e){

		    $('body').removeClass('dragging');
		    $('body').addClass('loading');

            $('.results').empty();

			e.preventDefault();
			var files = e.originalEvent.dataTransfer.files;

			// Closure to capture the file information.
			readSingleFile = function(e, file) {

				getTempo.getTempo( e.target.result, function(tempo){

                    $('body').removeClass('loading');

    				if ( !tempo ){
        				return $('.message').show().text('Something went wrong, please try a more modrern browser.');
    				}

                    $('.message').hide();
                    $('.results').append('<div class="result"><span class="name">' + file.name + ':</span><span class="tempo">' + tempo + 'BPM</span></div>');
    				console.log('Tempo: ' + tempo);
				});

			}

			var f;

            var filteredFiles = _.filter(files, function(file){
                return file.name.indexOf('.als') > -1;
            });

            if ( !filteredFiles.length ) {
                $('body').removeClass('loading');
                return $('.message').show().text('Must include an Ableton file in the drop (.als). Please try again.');
            }

			// loop trough files
			for (var i = 0; i < files.length; i++) {

                if ( files[i].name.indexOf('.als') == -1 ){
                    return;
                }

                // start reader
    			var reader = new FileReader();

			    // get item
			    f = files.item(i);

                reader.onloadend = (function(file) {
                    return function(e) {
                        readSingleFile(e, file)
                    };
                })(files[i]);

				// read file into buffer
				reader.readAsArrayBuffer(f);
			}

		});

	},

	getTempo: function(fileObject, cb){

		// if workers are supported...
		if ( $('html').hasClass('webworkers') ) {

			// start new worker
			var worker = new Worker( 'js/worker.js' );

			// when worker is finished...
			worker.addEventListener('message', function(e){

				// set data var
				var data = e.data;

				// use jQuery to parse xml as DOM
				try {

                    var $xml =  $( $.parseXML(data.tempo) );

                } catch(e) {

                    if ( typeof cb === 'function' ) cb(false);
                    return;

                }

				// get tempo value from xml
                var tempo = $xml.find('Tempo Manual').attr('Value');

				// fire callback
				if ( typeof cb === 'function' ) cb(tempo);

			});

			// send gzipped file to worker
			worker.postMessage({'fileObject' : fileObject });

		// if workers not supported...
		} else {

	        // start clock
	        var dateBefore = new Date();

	        // unpack zip file
	        var xmlString = pako.ungzip(fileObject, {to: 'string'});

	        // stop clock
	        var dateAfter = new Date();    
	
			// log time
	        // console.log("parsed in " + (dateAfter - dateBefore) + "ms");

            try {

	            // parse xml
    			var $xml =  $( $.parseXML(xmlString) );

            } catch(e) {

                if ( typeof cb === 'function' ) cb(false);
                return;

            }

			// get tempo value from xml
			var tempo = $xml.find('Tempo Manual').attr('Value');

			// fire callback
			if ( typeof cb === 'function' ) cb(tempo);

		}

	}

};
$(document).ready(function($){

	getTempo.init();

});